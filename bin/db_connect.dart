import 'package:mysql1/mysql1.dart';

Future<MySqlConnection> createConnection() async {
  final settings = ConnectionSettings(
    host: 'localhost',
    port: 3306,
    user: 'root',
    db: 'api_dart',
    password: null,
  );

  final connection = await MySqlConnection.connect(settings);

  return connection;
}
