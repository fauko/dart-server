-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2023 at 08:47 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_dart`
--

-- --------------------------------------------------------

--
-- Table structure for table `plate`
--

CREATE TABLE `plate` (
  `id_plate` int(11) NOT NULL,
  `policy_number` varchar(12) NOT NULL,
  `name_plate` varchar(30) NOT NULL,
  `email_owner` varchar(25) NOT NULL,
  `premium_amount` double NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `plate`
--

INSERT INTO `plate` (`id_plate`, `policy_number`, `name_plate`, `email_owner`, `premium_amount`, `start_date`, `end_date`) VALUES
(1, 'B 6990 TFR', 'fauko', 'fauko@gmail.com', 15000, '2023-11-02 10:31:07', '2024-11-14 10:31:07'),
(2, 'B 6980 EFR', 'fauko', 'fauko@gmail.com', 30000, '2023-11-02 11:55:34', '2023-11-02 11:55:34'),
(3, 'B 7050 ABC', 'koko', 'koko@gmail.com', 5000, '2023-11-02 14:16:23', '2023-11-02 14:16:23'),
(4, 'B 2910 TFY', 'ahmad sobari', 'sobari@gmail.com', 30000, '2023-11-02 14:16:23', '2025-11-02 14:16:23');

-- --------------------------------------------------------

--
-- Table structure for table `sesion`
--

CREATE TABLE `sesion` (
  `id_token` int(11) NOT NULL,
  `exp` varchar(50) NOT NULL,
  `token` text NOT NULL,
  `email_user` varchar(30) NOT NULL,
  `exp_date` text NOT NULL,
  `refresh` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sesion`
--

INSERT INTO `sesion` (`id_token`, `exp`, `token`, `email_user`, `exp_date`, `refresh`) VALUES
(183, '1699255922', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImZhdWtvQGdtYWlsLmNvbSIsImV4cCI6MTY5OTI1NTkyMn0=.iCjJx__ou7291BDH8D5rIku_9hqH56qbkH62whvp1As=', 'fauko@gmail.com', '2023-11-06 14:32:02.000', 'iCjJx__ou7291BDH8D5rIku_9hqH56qbkH62whvp1As=');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` int(35) NOT NULL,
  `name` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `status_kw` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `name`, `alamat`, `status_kw`, `email`, `password`) VALUES
(9, 8545546, 'fauko', 'depok', 'lajang', 'fauko@gmail.com', '123'),
(10, 2345435, 'koko', 'jakarta', 'lajang', 'koko@gmail.com', '123'),
(11, 3246567, 'ahmad anjar', 'bogor', 'nikah', 'ahmad@gmail.com', '123'),
(12, 33343, 'ahmad sobari', '', '', 'sobari@gmail.com', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `plate`
--
ALTER TABLE `plate`
  ADD PRIMARY KEY (`id_plate`);

--
-- Indexes for table `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `plate`
--
ALTER TABLE `plate`
  MODIFY `id_plate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sesion`
--
ALTER TABLE `sesion`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
