import 'dart:io';
import 'package:logger/logger.dart';
import 'package:mysql1/mysql1.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_router/shelf_router.dart' as appRoute;

import 'backend_rest_api/auth/login_user.api.dart';
import 'backend_rest_api/auth/register_user.api.dart';
import 'backend_rest_api/checking_token.dart';
import 'backend_rest_api/close_api.dart';
import 'backend_rest_api/get_index.dart';
import 'backend_rest_api/plate police/delete_policeNumber.dart';
import 'backend_rest_api/plate police/get_policeNumber.dart';
import 'backend_rest_api/plate police/post_policeNumber.dart';
import 'backend_rest_api/plate police/putEditPoliceNumber.dart';
import 'backend_rest_api/plate police/put_policeNumber.dart';
import 'backend_rest_api/refresh_token.dart';
import 'backend_rest_api/user/delete_user.dart';
import 'backend_rest_api/user/get_user.dart';
import 'backend_rest_api/user/put_user.dart';

final app = appRoute.Router();

Future<void> main() async {
  // app.post('/coba', handlercoba); // Route for getting
  app.get('/shutdown', closeapi); // Route for getting
  // auth api
  app.post('/login', login); // Route for getting login
  app.post('/cektoken', cekToken); // Route for getting token
  app.post('/refresh', refreshToken); // Route for getting token
  app.post('/daftar', userPostRequest); // Route for adding a user
  //user api
  app.get('/user', userGetRequest); // Route for getting users
  app.put('/user', userPutRequest); // Route for updating a user
  app.delete('/user', userDeleteRequest); // Route for deleting a user
  // plate police api
  app.get('/platepolice', platePoliceGetRequest); // get api plate police
  app.post('/platepolice', platePolicePostRequest); // get api plate police
  app.delete('/platepolice', platePoliceDeleteRequest);
  app.put('/platepolice', platePoliceEditRequest);
  app.put('/rubahplat', platePolicePutRequest);
  //
  Map<String, String> envVars = Platform.environment;
  // var portEnv = envVars['PORT'];
  // var PORT = portEnv == null ? 7777 : int.parse(portEnv);
  final handler = const Pipeline().addMiddleware(logRequests()).addHandler(app);
  //final serverDB = await shelf_io.serve(handler, '0.0.0.0', PORT);
  final serverDB = await shelf_io.serve(handler, '127.1.1.1', 8181);
  print('Listening on ${serverDB.address.host}:${serverDB.port}');
}
