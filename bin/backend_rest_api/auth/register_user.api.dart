import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> userPostRequest(Request request) async {
  if (!request.isMultipart) {
    final form = await request.readAsString();
    final formData = Uri.splitQueryString(form);

    final name = formData['nama'];

    final nomor = formData['email'];
    final password = formData['password'];
    if (name != null && nomor != null) {
      final connection = await createConnection();

      // Cek apakah data dengan nama atau nomor yang sama sudah ada
      final existingData = await connection
          .query('SELECT * FROM user WHERE email = ? ', [nomor]);

      if (existingData.isEmpty) {
        final result = await connection.query(
            'INSERT INTO user (name, email, password) VALUES (?, ? , ?)',
            [name, nomor, password]);
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok('Data berhasil ditambahkan ke MySQL',
              headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal menambahkan data ke MySQL', headers: cors);
        }
      } else {
        return Response.badRequest(
            body: 'Data dengan email yang sama sudah ada', headers: cors);
      }
    } else {
      return Response.badRequest(body: 'Invalid form data', headers: cors);
    }
  } else if (request.isMultipartForm) {
    String nama = '';
    String email = '';
    String password = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'email') {
        email = await formData.part.readString();
      } else if (formData.name == 'password') {
        password = await formData.part.readString();
      } else if (formData.name == 'nama') {
        nama = await formData.part.readString();
      }
    }
    if (email.isNotEmpty && password.isNotEmpty && nama.isNotEmpty) {
      final connection = await createConnection();

      // Cek apakah data dengan nama atau nomor yang sama sudah ada
      final existingData = await connection
          .query('SELECT * FROM user WHERE email = ? ', [email]);

      if (existingData.isEmpty) {
        final result = await connection.query(
            'INSERT INTO user (name, email, password) VALUES (?, ? , ?)',
            [nama, email, password]);
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok('Data berhasil ditambahkan ke MySQL',
              headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal menambahkan data ke MySQL', headers: cors);
        }
      } else {
        return Response.badRequest(
            body: 'Data dengan email yang sama sudah ada', headers: cors);
      }
    } else {
      return Response.badRequest(body: 'Invalid form data', headers: cors);
    }
  } else {
    return Response.ok('Regular multipart request', headers: cors);
  }
}
