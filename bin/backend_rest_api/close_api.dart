import 'dart:io';

import 'package:shelf/shelf.dart';

Future<Response> closeapi(Request request, HttpServer serverDB) async {
  await serverDB.close();
  return Response.ok("SERVER DOWN ",
      headers: {'Content-Type': 'application/json'});
}
