import 'dart:async';
import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:mysql1/src/single_connection.dart';
import 'package:shelf/shelf.dart';

import 'package:crypto/crypto.dart' as crypto;
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../db_connect.dart';
import 'cors.dart';

Future<Response> refreshToken(Request request) async {
  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;

  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header

  final connection = await createConnection();

  // Verifikasi token dengan memeriksa keberadaan token di tabel sesi
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }
  if (!request.isMultipart) {
    final body = await request.readAsString();
    final formData = Uri.splitQueryString(body);

    final refresh = formData['refresh'];
    if (refresh != null) {
      final connection = await createConnection();

      final existingData = await connection
          .query('SELECT * FROM sesion WHERE refresh = ?', [refresh]);

      // final existingData = await connection.query(
      //     'SELECT * FROM user WHERE email = ? OR password', [email, password]);
      if (existingData.isNotEmpty) {
        final payload = {
          'refresh': refresh,
          'exp': (DateTime.now()
                  .add(const Duration(hours: 24))
                  .toUtc()
                  .millisecondsSinceEpoch ~/
              1000),
        };

        final header = {
          'alg': 'HS256',
          'typ': 'JWT',
        };

        final base64UrlHeader =
            base64Url.encode(utf8.encode(json.encode(header)));
        final base64UrlPayload =
            base64Url.encode(utf8.encode(json.encode(payload)));

        final signature = hmacSha256(base64UrlHeader, base64UrlPayload);

        final token = '$base64UrlHeader.$base64UrlPayload.$signature';

        final tokendata = await connection
            .query('SELECT * FROM sesion WHERE refresh = ? ', [refresh]);
        final dateTime = DateTime.fromMillisecondsSinceEpoch((DateTime.now()
                    .add(const Duration(hours: 24))
                    .toUtc()
                    .millisecondsSinceEpoch ~/
                1000) *
            1000);

        if (DateTime.parse(tokendata.first[4].toString()).isBefore(dateTime)) {
          Logger().w(dateTime);
          Logger().w(
              DateTime.parse(tokendata.first[4].toString()).isAfter(dateTime));
          final result = await connection.query(
              'DELETE FROM sesion WHERE email_user = ?', [tokendata.first[3]]);
          // ulangi
          if (result.affectedRows! > 0) {
            final result = await connection.query(
                'INSERT INTO sesion (exp, token, email_user , exp_date, refresh) VALUES (?, ? ,? ,? ,?)',
                [
                  payload['exp'],
                  token,
                  tokendata.first[3],
                  dateTime.toString(),
                  signature
                ]);

            final getresults = await connection.query(
                'SELECT * FROM user WHERE email = ?', [tokendata.first[3]]);
            final data = getresults.map((row) {
              return {
                'id': row[0],
                'nik': row[1],
                'nama': row[2],
                'alamat': row[3].toString(),
                'status_kw': row[4],
                'email': row[5],
                // tambahkan lebih banyak kolom sesuai kebutuhan
              };
            }).toList();
            // List data = getresults.map((r) => r.fields).toList();

            final existingData = await connection.query(
                'SELECT * FROM plate WHERE email_owner = ? ',
                [tokendata.first[3]]);
            // List getDataPlate = existingData.map((r) => r.fields).toList();

            final getDataPlate = existingData.map((row) {
              return {
                'id_plate': row[0],
                'policy_number': row[1],
                'name_plate': row[2],
                'email_owner': row[3],
                'premium_amount': row[4],
                'start_date': row[5].toString(),
                'end_date': row[6].toString(),
                // tambahkan lebih banyak kolom sesuai kebutuhan
              };
            }).toList();
            if (result.affectedRows! > 0) {
              await connection.close();
              var send = {
                'token': token,
                'refresh': signature,
                'user': data,
                'policynumber': getDataPlate,
              };
              Logger().w(json.encode(send['token']));

              return Response.ok(
                json.encode(send),
                headers: cors,
              );
            }
            {
              return Response.forbidden("server erorr");
            }
          } else {
            return Response.forbidden("False 1");
          }
        } else {
          return Response.internalServerError(body: 'Login failed 2');
        }
      } else {
        return Response.internalServerError(body: 'Login failed1');
      }
    } else {
      return Response.badRequest(body: 'Missing email or password');
    }
  } else if (request.isMultipartForm) {
    String refresh = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'refresh') {
        refresh = await formData.part.readString();
      }
    }
    if (refresh.isNotEmpty) {
      final connection = await createConnection();

      final existingData = await connection
          .query('SELECT * FROM sesion WHERE refresh = ? ', [refresh]);
      Logger().w("ping");

      // final existingData = await connection.query(
      //     'SELECT * FROM user WHERE email = ? OR password', [email, password]);
      if (existingData.isNotEmpty) {
        final payload = {
          'refresh': refresh,
          'exp': (DateTime.now()
                  .add(const Duration(hours: 24))
                  .toUtc()
                  .millisecondsSinceEpoch ~/
              1000),
        };

        final header = {
          'alg': 'HS256',
          'typ': 'JWT',
        };

        final base64UrlHeader =
            base64Url.encode(utf8.encode(json.encode(header)));
        final base64UrlPayload =
            base64Url.encode(utf8.encode(json.encode(payload)));

        final signature = hmacSha256(base64UrlHeader, base64UrlPayload);

        final token = '$base64UrlHeader.$base64UrlPayload.$signature';

        final tokendata = await connection
            .query('SELECT * FROM sesion WHERE refresh = ? ', [refresh]);
        final dateTime = DateTime.fromMillisecondsSinceEpoch((DateTime.now()
                    .add(const Duration(hours: 24))
                    .toUtc()
                    .millisecondsSinceEpoch ~/
                1000) *
            1000);
        if (DateTime.parse(tokendata.first[4].toString()).isBefore(dateTime)) {
          Logger().w(dateTime);
          Logger().w(
              DateTime.parse(tokendata.first[4].toString()).isAfter(dateTime));
          final result = await connection.query(
              'DELETE FROM sesion WHERE email_user = ?', [tokendata.first[3]]);
          // ulangi
          if (result.affectedRows! > 0) {
            final result = await connection.query(
                'INSERT INTO sesion (exp, token, email_user , exp_date, refresh) VALUES (?, ? ,? ,? ,?)',
                [
                  payload['exp'],
                  token,
                  tokendata.first[3],
                  dateTime.toString(),
                  signature
                ]);

            final getresults = await connection.query(
                'SELECT * FROM user WHERE email = ?', [tokendata.first[3]]);
            final data = getresults.map((row) {
              return {
                'id': row[0],
                'nik': row[1],
                'nama': row[2],
                'alamat': row[3].toString(),
                'status_kw': row[4],
                'email': row[5],
                // tambahkan lebih banyak kolom sesuai kebutuhan
              };
            }).toList();
            // List data = getresults.map((r) => r.fields).toList();

            final existingData = await connection.query(
                'SELECT * FROM plate WHERE email_owner = ? ',
                [tokendata.first[3]]);
            // List getDataPlate = existingData.map((r) => r.fields).toList();

            final getDataPlate = existingData.map((row) {
              return {
                'id_plate': row[0],
                'policy_number': row[1],
                'name_plate': row[2],
                'email_owner': row[3],
                'premium_amount': row[4],
                'start_date': row[5].toString(),
                'end_date': row[6].toString(),
                // tambahkan lebih banyak kolom sesuai kebutuhan
              };
            }).toList();
            if (result.affectedRows! > 0) {
              await connection.close();
              var send = {
                'token': token,
                'refresh': signature,
                'user': data,
                'policynumber': getDataPlate,
              };
              Logger().w(json.encode(send['token']));

              return Response.ok(json.encode(send), headers: cors);
            }
            {
              return Response.forbidden("server erorr");
            }
          } else {
            return Response.forbidden("False 1");
          }
        } else {
          return Response.internalServerError(body: 'Login failed 2');
        }
      } else {
        return Response.internalServerError(body: 'Login failed1');
      }
    } else {
      return Response.badRequest(body: 'Missing email or password');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}

String hmacSha256(String data, String key) {
  final hmac = crypto.Hmac(crypto.sha256, utf8.encode(key));
  final digest = hmac.convert(utf8.encode(data));
  return base64Url.encode(digest.bytes);
}
