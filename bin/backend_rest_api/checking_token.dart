import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../db_connect.dart';
import 'cors.dart';

Future<Response> cekToken(Request request) async {
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;

  ///
  if (!request.isMultipart) {
    final body = await request.readAsString();

    final formData = Uri.splitQueryString(body);

    final token = formData['token'];

    if (token != null) {
      final connection = await createConnection();

      final existingData = await connection
          .query('SELECT * FROM sesion WHERE token = ? ', [token]);
      if (existingData.isNotEmpty) {
        final row = existingData.first;

        if (int.parse(row[1]) > currentTime) {
          await connection.close();

          return Response.ok(
            json.encode({
              'token': row[2].toString(),
              'refresh token': row[5].toString(),
              'email': row[3]
            }),
            headers: cors,
          );
        } else {
          return Response.forbidden('Token telah kedaluwarsa');
        }
      } else {
        return Response.forbidden('Token tidak tersedia');
      }
    } else {
      return Response.forbidden('Token belum terinput');
    }
  } else if (request.isMultipartForm) {
    String token = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'token') {
        token = await formData.part.readString();
      }
    }
    if (token.isNotEmpty) {
      final connection = await createConnection();

      final existingData = await connection
          .query('SELECT * FROM sesion WHERE token = ? ', [token]);
      if (existingData.isNotEmpty) {
        final row = existingData.first;

        if (int.parse(row[1]) > currentTime) {
          await connection.close();

          return Response.ok(
            json.encode({
              'token': row[2].toString(),
              'refresh token': row[5].toString(),
              'email': row[3]
            }),
            headers: cors
          );
        } else {
          return Response.forbidden('Token telah kedaluwarsa');
        }
      } else {
        return Response.forbidden('Token tidak tersedia');
      }
    } else {
      return Response.forbidden('Token belum terinput');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}
