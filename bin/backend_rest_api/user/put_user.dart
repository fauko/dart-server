import 'dart:convert';
import 'dart:math';

import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> userPutRequest(Request request) async {
  final connection = await createConnection();

  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;
  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }

  //
  if (!request.isMultipart) {
    final form = await request.readAsString();
    final formData = Uri.splitQueryString(form);
    final id = formData['id_user'];
    final nik = formData['nik'];
    final name = formData['nama'];
    final alamat = formData['alamat'];
    final status = formData['status'];
    final email = formData['email'];
    final password = formData['password'];

    final columnsToEdit = <String>[]; // Daftar kolom yang akan diubah
    final values = <dynamic>[]; // Nilai baru untuk kolom-kolom yang diubah

    if (id != null) {
      if (nik != null) {
        columnsToEdit.add('nik');
        values.add(nik);
      }
      if (name != null) {
        columnsToEdit.add('name');
        values.add(name);
      }
      if (alamat != null) {
        columnsToEdit.add('alamat');
        values.add(alamat);
      }
      if (status != null) {
        columnsToEdit.add('status_kw');
        values.add(status);
      }
      if (email != null) {
        columnsToEdit.add('email');
        values.add(email);
      }
      if (password != null) {
        columnsToEdit.add('password');
        values.add(password);
      }

      if (columnsToEdit.isNotEmpty &&
          tokenHeader.isNotEmpty &&
          tokenQuery.isNotEmpty &&
          int.parse(tokenQuery.first[1]) > currentTime) {
        final query =
            'UPDATE user SET ${columnsToEdit.map((column) => '$column = ?').join(', ')} WHERE id_user = ?';

        values.add(id);
        final result = await connection.query(query, values);

        final results = await connection
            .query('SELECT * FROM user WHERE id_user = ?', [id]);
        final data = results.map((row) {
          return {
            'status': "succes",
            'id': row[0],
            'nik': row[1],
            'nama': row[2],
            'alamat': row[3].toString(),
            'status_kw': row[4],
            'email': row[5],
            // tambahkan lebih banyak kolom sesuai kebutuhan
          };
        }).toList();
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok(json.encode(data), headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal mengupdate data di MySQL');
        }
      } else {
        return Response.badRequest(body: 'Tidak ada data yang diubah');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else if (request.isMultipartForm) {
    String id = '';
    String nik = '';
    String name = '';
    String alamat = '';
    String status = '';
    String email = '';
    String password = '';
    final columnsToEdit = <String>[]; // Daftar kolom yang akan diubah
    final values = <dynamic>[]; // Nilai baru untuk kolom-kolom yang diubah

    await for (final formData in request.multipartFormData) {
      if (formData.name == 'id_user') {
        id = await formData.part.readString();
      } else if (formData.name == 'nik') {
        nik = await formData.part.readString();
      } else if (formData.name == 'nama') {
        name = await formData.part.readString();
      } else if (formData.name == 'alamat') {
        alamat = await formData.part.readString();
      } else if (formData.name == 'status') {
        status = await formData.part.readString();
      } else if (formData.name == 'email') {
        email = await formData.part.readString();
      } else if (formData.name == 'password') {
        password = await formData.part.readString();
      }
    }
    if (id.isNotEmpty) {
      if (nik.isNotEmpty) {
        columnsToEdit.add('nik');
        values.add(nik);
      }
      if (name.isNotEmpty) {
        columnsToEdit.add('name');
        values.add(name);
      }
      if (alamat.isNotEmpty) {
        columnsToEdit.add('alamat');
        values.add(alamat);
      }
      if (status.isNotEmpty) {
        columnsToEdit.add('status_kw');
        values.add(status);
      }
      if (email.isNotEmpty) {
        columnsToEdit.add('email');
        values.add(email);
      }
      if (password.isNotEmpty) {
        columnsToEdit.add('password');
        values.add(password);
      }

      if (columnsToEdit.isNotEmpty &&
          tokenHeader.isNotEmpty &&
          tokenQuery.isNotEmpty &&
          int.parse(tokenQuery.first[1]) > currentTime) {
        final query =
            'UPDATE user SET ${columnsToEdit.map((column) => '$column = ?').join(', ')} WHERE id_user = ?';

        values.add(id);
        final result = await connection.query(query, values);

        final results = await connection
            .query('SELECT * FROM user WHERE id_user = ?', [id]);
        final data = results.map((row) {
          return {
            'status': "succes",
            'id': row[0],
            'nik': row[1],
            'nama': row[2],
            'alamat': row[3].toString(),
            'status_kw': row[4],
            'email': row[5],
            // tambahkan lebih banyak kolom sesuai kebutuhan
          };
        }).toList();
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok(json.encode(data), headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal mengupdate data di MySQL');
        }
      } else {
        return Response.badRequest(body: 'Tidak ada data yang diubah');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}
