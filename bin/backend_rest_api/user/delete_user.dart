import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> userDeleteRequest(Request request) async {
  final connection = await createConnection();

  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;
  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }
  //
  if (!request.isMultipart) {
    final form = await request.readAsString();
    final formData = Uri.splitQueryString(form);
    final id = formData['id_user'];

    if (id != null) {
      final result =
          await connection.query('DELETE FROM user WHERE id_user = ?', [id]);
      await connection.close();

      if (result.affectedRows! > 0) {
        return Response.ok('Data berhasil dihapus dari MySQL',
            headers: cors);
      } else {
        return Response.internalServerError(
            body: 'Gagal menghapus data dari MySQL');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else if (request.isMultipartForm) {
    String id = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'id_user') {
        id = await formData.part.readString();
      }
    }
    if (id.isNotEmpty) {
      final result =
          await connection.query('DELETE FROM user WHERE id_user = ?', [id]);
      await connection.close();

      if (result.affectedRows! > 0) {
        return Response.ok('Data berhasil dihapus dari MySQL',
            headers: cors);
      } else {
        return Response.internalServerError(
            body: 'Gagal menghapus data dari MySQL');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}
