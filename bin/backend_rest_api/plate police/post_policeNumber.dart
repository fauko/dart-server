import 'package:logger/logger.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> platePolicePostRequest(Request request) async {
  final connection = await createConnection();

  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;
  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }

  //
  if (!request.isMultipart) {
    final form = await request.readAsString();
    final formData = Uri.splitQueryString(form);
    final policyNumber = formData['policy_number'];
    final name = formData['name_plate'];
    final emailOwner = formData['email_owner'];
    final premiumNumber = formData['premium_amount'];
    final startDate = formData['start_date'];
    final endDate = formData['end_date'];

    if (name != null &&
        policyNumber != null &&
        premiumNumber != null &&
        startDate != null &&
        endDate != null &&
        emailOwner != null) {
      // Cek apakah data dengan nama atau nomor yang sama sudah ada
      final existingData = await connection.query(
          'SELECT * FROM plate WHERE policy_number = ? ', [policyNumber]);

      if (existingData.isEmpty) {
        final result = await connection.query(
            'INSERT INTO plate (policy_number, name_plate, email_owner, premium_amount, start_date, end_date) VALUES (?, ? , ?,?,?,?)',
            [
              policyNumber,
              name,
              emailOwner,
              premiumNumber,
              startDate,
              endDate
            ]);
        await connection.close();
        if (result.affectedRows! > 0) {
          await connection.close();

          return Response.ok('Data berhasil ditambahkan ke MySQL',
              headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal menambahkan data ke MySQL');
        }
      } else {
        return Response.badRequest(
            body: 'Data dengan nomor polisi yang sama sudah ada');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else if (request.isMultipartForm) {
    //     final policyNumber = formData['policy_number'];
    // final name = formData['name_plate'];
    // final emailOwner = formData['email_owner'];
    // final premiumNumber = formData['premium_amount'];
    // final startDate = formData['start_date'];
    // final endDate = formData['end_date'];
    String policyNumber = '';
    String name = '';
    String emailOwner = '';
    String premiumNumber = '';
    String startDate = '';
    String endDate = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'policy_number') {
        policyNumber = await formData.part.readString();
      } else if (formData.name == 'name_plate') {
        name = await formData.part.readString();
      } else if (formData.name == 'email_owner') {
        emailOwner = await formData.part.readString();
      } else if (formData.name == 'premium_amount') {
        premiumNumber = await formData.part.readString();
      } else if (formData.name == 'start_date') {
        startDate = await formData.part.readString();
      } else if (formData.name == 'end_date') {
        endDate = await formData.part.readString();
      }
    }
    if (name.isNotEmpty &&
        policyNumber.isNotEmpty &&
        premiumNumber.isNotEmpty &&
        startDate.isNotEmpty &&
        endDate.isNotEmpty &&
        emailOwner.isNotEmpty) {
      // Cek apakah data dengan nama atau nomor yang sama sudah ada
      final existingData = await connection.query(
          'SELECT * FROM plate WHERE policy_number = ? ', [policyNumber]);

      if (existingData.isEmpty) {
        final result = await connection.query(
            'INSERT INTO plate (policy_number, name_plate, email_owner, premium_amount, start_date, end_date) VALUES (?, ? , ?,?,?,?)',
            [
              policyNumber,
              name,
              emailOwner,
              premiumNumber,
              startDate,
              endDate
            ]);
        await connection.close();

        if (result.affectedRows! > 0) {
          await connection.close();

          return Response.ok('Data berhasil ditambahkan ke MySQL',
              headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal menambahkan data ke MySQL' ,headers: cors);
        }
      } else {
        return Response.badRequest(
            body: 'Data dengan nomor polisi yang sama sudah ada');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}
