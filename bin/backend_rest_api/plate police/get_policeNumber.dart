import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:shelf/shelf.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> platePoliceGetRequest(Request request) async {
  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;

  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header

  final connection = await createConnection();

  // Verifikasi token dengan memeriksa keberadaan token di tabel sesi
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }

  final results = await connection.query('SELECT * FROM plate');
    final data = results.map((row) {
    return {
      'id_plate': row[0],
      'policy_number': row[1],
      'name_plate': row[2],
      'email_owner': row[3],
      'premium_amount': row[4],
      'start_date': row[5].toString(),
      'end_date': row[6].toString(),
      // tambahkan lebih banyak kolom sesuai kebutuhan
    };
  }).toList();

  await connection.close();
  final jsonResponse = json.encode(data);

  // Logger().w(data);
  // Logger().w(jsonResponse);
  return Response.ok(jsonResponse,
      headers: cors);
}
