import 'package:shelf/shelf.dart';
import 'package:shelf_multipart/form_data.dart';
import 'package:shelf_multipart/multipart.dart';

import '../../db_connect.dart';
import '../cors.dart';

Future<Response> platePolicePutRequest(Request request) async {
  final connection = await createConnection();

  final headers = request.headers;
  final tokenHeader = headers['Authorization'];
  final currentTime = DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000;
  if (tokenHeader == null || !tokenHeader.startsWith('Bearer ')) {
    return Response.forbidden('Token tidak valid atau tidak tersedia');
  }

  final token =
      tokenHeader.replaceFirst('Bearer ', ''); // Mengambil token dari header
  final tokenQuery =
      await connection.query('SELECT * FROM sesion WHERE token = ?', [token]);
  if (tokenQuery.isEmpty) {
    await connection.close();
    return Response.forbidden('Token tidak valid');
  }
  // Logger().w(rowdata[1]);
  if (int.parse(tokenQuery.first[1]) < currentTime) {
    await connection.close();
    return Response.forbidden('Token kedaluwarsa');
  }

  ///
  if (!request.isMultipart) {
    final form = await request.readAsString();
    final formData = Uri.splitQueryString(form);
    final id = formData['id_plate'];
    final policyNumber = formData['policy_number'];
    final name = formData['name_plate'];
    final emailowner = formData['email_owner'];
    final premiumNumber = formData['premium_amount'];
    final startDate = formData['start_date'];
    final endDate = formData['end_date'];

    final columnsToEdit = <String>[]; // Daftar kolom yang akan diubah
    final values = <dynamic>[]; // Nilai baru untuk kolom-kolom yang diubah

    if (id != null) {
      if (policyNumber != null) {
        columnsToEdit.add('policy_number');
        values.add(policyNumber);
      }
      if (name != null) {
        columnsToEdit.add('name_plate');
        values.add(name);
      }
      if (emailowner != null) {
        columnsToEdit.add('email_owner');
        values.add(emailowner);
      }
      if (premiumNumber != null) {
        columnsToEdit.add('premium_amount');
        values.add(premiumNumber);
      }
      if (startDate != null) {
        columnsToEdit.add('start_date');
        values.add(startDate);
      }
      if (endDate != null) {
        columnsToEdit.add('end_date');
        values.add(endDate);
      }

      if (columnsToEdit.isNotEmpty) {
        final query =
            'UPDATE plate SET ${columnsToEdit.map((column) => '$column = ?').join(', ')} WHERE id_plate = ?';

        values.add(id);

        final result = await connection.query(query, values);
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok('Data berhasil diupdate di MySQL', headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal mengupdate data di MySQL', headers: cors);
        }
      } else {
        return Response.badRequest(body: 'Tidak ada data yang diubah');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else if (request.isMultipartForm) {
    String idplate = '';
    String policyNumber = '';
    String name = '';
    String emailOwner = '';
    String premiumNumber = '';
    String startDate = '';
    String endDate = '';
    await for (final formData in request.multipartFormData) {
      if (formData.name == 'id_plate') {
        idplate = await formData.part.readString();
      } else if (formData.name == 'policy_number') {
        policyNumber = await formData.part.readString();
      } else if (formData.name == 'name_plate') {
        name = await formData.part.readString();
      } else if (formData.name == 'email_owner') {
        emailOwner = await formData.part.readString();
      } else if (formData.name == 'premium_amount') {
        premiumNumber = await formData.part.readString();
      } else if (formData.name == 'start_date') {
        startDate = await formData.part.readString();
      } else if (formData.name == 'end_date') {
        endDate = await formData.part.readString();
      }
    }
    final columnsToEdit = <String>[]; // Daftar kolom yang akan diubah
    final values = <dynamic>[]; // Nilai baru untuk kolom-kolom yang diubah

    if (idplate.isNotEmpty) {
      if (policyNumber.isNotEmpty) {
        columnsToEdit.add('policy_number');
        values.add(policyNumber);
      }
      if (name.isNotEmpty) {
        columnsToEdit.add('name_plate');
        values.add(name);
      }
      if (emailOwner.isNotEmpty) {
        columnsToEdit.add('email_owner');
        values.add(emailOwner);
      }
      if (premiumNumber.isNotEmpty) {
        columnsToEdit.add('premium_amount');
        values.add(premiumNumber);
      }
      if (startDate.isNotEmpty) {
        columnsToEdit.add('start_date');
        values.add(startDate);
      }
      if (endDate.isNotEmpty) {
        columnsToEdit.add('end_date');
        values.add(endDate);
      }

      if (columnsToEdit.isNotEmpty) {
        final query =
            'UPDATE plate SET ${columnsToEdit.map((column) => '$column = ?').join(', ')} WHERE id_plate = ?';

        values.add(idplate);

        final result = await connection.query(query, values);
        await connection.close();

        if (result.affectedRows! > 0) {
          return Response.ok('Data berhasil diupdate di MySQL', headers: cors);
        } else {
          return Response.internalServerError(
              body: 'Gagal mengupdate data di MySQL');
        }
      } else {
        return Response.badRequest(body: 'Tidak ada data yang diubah');
      }
    } else {
      return Response.badRequest(body: 'Invalid form data');
    }
  } else {
    return Response.ok('Regular multipart request');
  }
}
